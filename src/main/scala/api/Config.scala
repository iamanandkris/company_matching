package api

case class Config(fileConfig: FileConfig, httpConfig:HttpConfig, urlEndpoints:UrlEndpoints)

case class FileConfig(filePathToSaveProcessedInfo:String,
                      processedFileExtension:String,
                      pathToMatcherDataWithFileName:String)
case class HttpConfig(host:String, port:Int)
case class UrlEndpoints(upload:String,getResult:String,getStatus:String,isWorkFinished:String, base:String)
