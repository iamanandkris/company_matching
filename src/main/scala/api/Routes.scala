package api

import java.nio.file.{Files, Paths}
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.Multipart.{BodyPart, FormData}
import akka.http.scaladsl.server._
import akka.pattern.ask
import akka.stream.Materializer
import akka.stream.scaladsl.FileIO
import akka.util.Timeout
import api.RouteUtils._
import com.typesafe.scalalogging.LazyLogging
import server.WorkCollatorFSM.{CurrentStatus, GetCurrentStatus}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

object Routes extends LazyLogging {
  implicit val getCurrentStatusTimeout = Timeout(3 seconds)
  implicit val formExtractionTimeout = FiniteDuration(3, TimeUnit.SECONDS)
  
  /**
   * This route processes the user input - User Data and Threshold.The input CSV is processed in a streamed fashion
   * It writes the result in to a file as per the config. It also send the current status to a collator actor.
   *
   * @param matcherData the company data that was loaded at the server startup.
   * @param config      the config loaded from application.conf
   * @return the csv uploader route
   */
  def uploadCSVRoute(matcherData: Future[List[Map[String, String]]], config: api.Config)
                    (implicit actorSystem: ActorSystem): Route = extractRequestContext { ctx =>
    implicit val ec = ctx.executionContext
    pathPrefix(config.urlEndpoints.upload) {
      entity(as[FormData]) { formData =>
        onComplete(performMatching(separateIncomingFormData(formData),
          matcherData,
          (x:String) => FileIO.toPath(Paths.get(s"${config.fileConfig.filePathToSaveProcessedInfo}${x}${config.fileConfig.processedFileExtension}")))) {
          case Success(value) => complete(getStatusResponse(s"""{"uniqueId" : "${value}","possibleActions" : {"getStatus" : "http://${config.httpConfig.host}:${config.httpConfig.port}/companyMatch/getStatus/${value}","isWorkFinished" : "http://${config.httpConfig.host}:${config.httpConfig.port}/companyMatch/isWorkFinished/${value}"}}"""))
          case Failure(ex) => complete(getStatusResponse(s"""{"uniqueId" : "","status":{"error":"${ex.getMessage}"}}"""))
        }
      }
    }
  }

  /**
   * Ths route tries to get the current processing status from the collator actor. It uses actor selection for this
   * @param config the config loaded from application.conf
   * @return the get current status route.
   */
  def getCurrentStatusRoute(config: api.Config)(implicit as: ActorSystem): Route =
    getterRoute(config.urlEndpoints.getStatus,
      (uniqueId: String) => as.actorSelection(s"akka://${as.name}/user/${uniqueId}").ask(GetCurrentStatus).mapTo[CurrentStatus],
      config,
      getCurrentStatusSuccessResponse(config))

  /**
   * This route tries to inform the user if the processing is finished or not, from the collator actor. It uses actor selection for this
   * @param config the config loaded from application.conf
   * @return the is work finished route
   */
  def isWorkFinishedRoute(config: api.Config)(implicit as: ActorSystem): Route =
    getterRoute(config.urlEndpoints.isWorkFinished,
      (uniqueId: String) => as.actorSelection(s"akka://${as.name}/user/${uniqueId}").ask(GetCurrentStatus).mapTo[CurrentStatus],
      config,
      isWorkFinishedSuccessResponse(config))


  /**
   * This route accesses the file that the uploadCSV route is writing/completed writing to stream the result back to the user.
   * @param config the config loaded from application.conf
   * @return the result Route
   */
  def getResultRoute(config: api.Config): Route =
    getterRoute(config.urlEndpoints.getResult,
      validateFileExistenceAndGetTheSource(config, path => Files.exists(path)),
      config,
      getCSVResponse)

  /**
   * The general purpose route creator for getStatus, isFinished and getResult
   * @param routeName name of the route
   * @param taskToPerform the task to perform as part of the route
   * @param config config
   * @param successResponse What should be the HTTPResponse on success
   * @tparam A This varies as per the requirement
   * @return A valid Route
   */
  private def getterRoute[A](routeName:String, taskToPerform:String => Future[A], config:Config,
                     successResponse:(String, A) => HttpResponse):Route ={
    pathPrefix(routeName / Segment) { uniqueRequestId =>
      onComplete(taskToPerform(uniqueRequestId)){
        case Success(currentStatus) => complete(successResponse(uniqueRequestId, currentStatus))
        case Failure(ex)    => {
          logger.error(s"getterRoute failure - ${ex.getMessage}")
          complete(statusInquiryFailureResponse(uniqueRequestId))
        }
      }
    }
  }

  /**
   * Parsing the incoming form data to get threshold and input CSV fields extracted
   * @param formData
   * @param executionContext the future execution context.
   * @param mat stream materializer
   * @param timeOut
   * @return a future of Map. Expecting 2 entries in the map. One for CSV and one for threshold
   */
  def separateIncomingFormData(formData: FormData)
                              (implicit executionContext: ExecutionContext,
                               mat: Materializer,
                               timeOut: FiniteDuration)
  : Future[Map[String, Any]] = {
    formData.parts
      .mapAsyncUnordered(2)({
        case file if file.name == "csv" => Future.successful("uploadedFile" -> file.entity.dataBytes)
        case b: BodyPart if b.name == "threshold" => b.toStrict(timeOut).map(strict => "threshold" -> strict.entity.data.utf8String)
        case b: BodyPart => b.toStrict(timeOut).map(strict => b.name -> strict.entity.data.utf8String)
      })
      .runFold(Map.empty[String, Any]) {
        case (map, (key, value)) => map.updated(key, value)
      }
  }
}