package api

import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.scaladsl.FileIO
import api.Routes._
import com.typesafe.scalalogging.LazyLogging
import pureconfig.ConfigSource
import stream.StreamsUtil
import pureconfig.generic.auto._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try


object AkkaHTTPServer extends App with Directives with LazyLogging{
  implicit val actorSystem:ActorSystem = ActorSystem("FileUploadProcessorActorSystem")

  /**
   * Loading and parsing the configuration
   */
  lazy val matcherProgramConfig: api.Config = ConfigSource.fromConfig(actorSystem.settings.config).loadOrThrow[api.Config]
  logger.info(s"Loaded Configuration - ${matcherProgramConfig}")

  /**
   * A one time processing activity as part of the server start up to keep the company data in the memory considering
   * that the max is 10k
   */
  val companyData: Future[List[Map[String, String]]] = for {
    file <- Future.fromTry(Try(Paths.get(matcherProgramConfig.fileConfig.pathToMatcherDataWithFileName)))
    data <- StreamsUtil.getCompanyData(FileIO.fromPath(file))
  } yield data


  /**
   * Http server biding to host and port with available routes
   */
  Http().bindAndHandle(pathPrefix(matcherProgramConfig.urlEndpoints.base) {
    uploadCSVRoute(companyData, matcherProgramConfig) ~
    getCurrentStatusRoute(matcherProgramConfig) ~
    isWorkFinishedRoute((matcherProgramConfig)) ~
    getResultRoute(matcherProgramConfig)
  }, matcherProgramConfig.httpConfig.host, matcherProgramConfig.httpConfig.port)
}
