package api

import java.nio.file.{Files, Path, Paths}
import java.util.UUID

import akka.actor.{ActorSelection, ActorSystem}
import akka.http.scaladsl.model.HttpEntity.{ChunkStreamPart, Chunked}
import akka.http.scaladsl.model.Multipart.{BodyPart, FormData}
import akka.http.scaladsl.model.headers.{ContentDispositionTypes, `Content-Disposition`}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse}
import akka.http.scaladsl.server.{Route, _}
import akka.stream.scaladsl.{FileIO, Sink, Source}
import akka.stream.{IOResult, Materializer}
import akka.util.ByteString
import server.WorkCollatorFSM
import server.WorkCollatorFSM.CurrentStatus
import stream.StreamsUtil

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

object RouteUtils extends Directives {
  case class UploadFormInformation(threshold:Double, fileSource:Source[ByteString, Any])

  /**
   * This function validates file path and returns the Source
   * @param config config
   * @param fileCheck a function pushed from outside to check the file existence.
   * @param uniqueId unique id of the request
   * @return The source that can be sink-ed to the user browser
   */
  def validateFileExistenceAndGetTheSource(config:Config, fileCheck:Path => Boolean)(uniqueId:String):Future[Source[ByteString, Any]]={
    val filePath = Paths.get(s"""${config.fileConfig.filePathToSaveProcessedInfo}${uniqueId}${config.fileConfig.processedFileExtension}""")
    if (fileCheck(filePath)) {
      Future.successful(FileIO.fromPath(filePath))
    } else {
      Future.failed {
        new Exception("File does not exist")
      }
    }
  }

  def getCurrentStatusSuccessResponse(config:Config)(uniqueId:String, currentStatus:CurrentStatus):HttpResponse =getStatusResponse(
    s"""{"uniqueId" : "${uniqueId}","status":{"rowsCameIn":${currentStatus.currentTotal},"rowsProcessedSuccess":${currentStatus.success},"rowsProcessedFailure":${currentStatus.failure}},"possibleActions" : {"isWorkFinished" : "http://${config.httpConfig.host}:${config.httpConfig.port}/companyMatch/isWorkFinished/${uniqueId}"}}""")

  def isWorkFinishedSuccessResponse(config:Config)(uniqueId:String, currentStatus: CurrentStatus):HttpResponse =getStatusResponse(
    s"""{"uniqueId" : "${uniqueId}","workFinishStatus":${currentStatus.isFullyDone},"possibleActions" : {"getResult" : "http://${config.httpConfig.host}:${config.httpConfig.port}/companyMatch/getResult/${uniqueId}"}}""")
  def statusInquiryFailureResponse(uniqueId:String):HttpResponse =getStatusResponse(s"""{"uniqueId" : "${uniqueId}","status":{"error":"invalid request id"}}""")


  /**
   * This function extracts and validates FormData that was uploaded.
   * @param formDataMap the map created from fields of form data
   * @param executionContext
   * @param mat
   * @param timeOut
   * @return The UploadFormInformation that tells about the threshold value and SourceStream
   */
  def extractAndValidateFormInformation(formDataMap: Future[Map[String, Any]])
                                       (implicit executionContext: ExecutionContext,
                                        mat: Materializer,
                                        timeOut: FiniteDuration):Future[UploadFormInformation]={
    for{
      formDataMap <- formDataMap
      threshold <- formDataMap.get("threshold").map(Future.successful)
        .getOrElse(Future.failed(new Exception("Form field threshold not set")))
      thresholdDouble <- Future.fromTry(Try(threshold.toString.toDouble))
      validatedThreshold <- if(thresholdDouble < 0 || thresholdDouble > 1)
        Future.failed(new Exception("Threshold not within the neded range 0-1")) else Future.successful(thresholdDouble)
      uploadedFile <- formDataMap.get("uploadedFile").map(Future.successful)
        .getOrElse(Future.failed(new Exception("Could not start the file uploading. Something wrong with the CSV field/file")))
      uploadedFileSource <- Future.fromTry(Try(uploadedFile.asInstanceOf[Source[ByteString, Any]]))
    }yield UploadFormInformation(validatedThreshold, uploadedFileSource)
  }

  /**
   * The high level matching function that iterates over the user stream and matches each row against the company data
   * @param userData The user input
   * @param companyData the data that is in company's store
   * @param sink the stream sink where the matched result has to be written
   * @param actorSystem Akka Acto system
   * @param executionContext
   * @param mat
   * @param timeOut
   * @param uuid a UUID generator.
   * @return The uuid of the currently executed request.
   */
  def performMatching(userData: Future[Map[String, Any]],
                      companyData: Future[List[Map[String, String]]],
                      sink:String =>Sink[ByteString, Any])
                    (implicit actorSystem:ActorSystem,
                     executionContext: ExecutionContext,
                     mat: Materializer,
                     timeOut: FiniteDuration,
                     uuid:GenerateUUID):Future[String]={
    for{
      uploadFormInfo <- extractAndValidateFormInformation(userData)
      companyData <- companyData
      uniqueIdToUse <- Future.successful(uuid())
      collatorFSM <- Future.successful(actorSystem.actorOf(WorkCollatorFSM.props, uniqueIdToUse))
      totalCount <- StreamsUtil.sendToPerformMatching(uploadFormInfo.fileSource,
        collatorFSM,
        companyData,
        uploadFormInfo.threshold,
        sink(uniqueIdToUse))
      _ <- Future.successful(collatorFSM ! WorkCollatorFSM.TotalCount(totalCount))
    }yield(uniqueIdToUse)
  }

  /**
   * The return content type setting as JSON
   * @param input the body
   * @return HTTP entity
   */
  def getStatusResponse(input:String):HttpResponse = HttpResponse(entity = HttpEntity(ContentTypes.`application/json`, input))

  /**
   * This function gets the CSV response from the input fileSource
   * @param fileName name that has to be set for the file while downloading
   * @param fileSource the source that has to be sink-ed to the browser
   * @return
   */
  def getCSVResponse(fileName:String, fileSource:Source[ByteString, Any])=
    HttpResponse(
      headers = `Content-Disposition`(ContentDispositionTypes.inline, Map("filename" -> s"$fileName.csv")) :: Nil,
      entity = Chunked(contentType = ContentTypes.`text/csv(UTF-8)`, chunks = fileSource.map(ChunkStreamPart.apply)))
}
