package api

import java.util.UUID

trait GenerateUUID extends (() => String)
object GenerateUUID {
  def apply()(implicit uuidGen: GenerateUUID): String = uuidGen()

  implicit val defaultUUIDgenerator: GenerateUUID =
    new GenerateUUID {
      override def apply(): String =
        UUID.randomUUID().toString
    }
}