package stream

import java.nio.file.Path

import akka.actor.ActorRef
import akka.stream.Materializer
import akka.stream.alpakka.csv.scaladsl.{CsvFormatting, CsvParsing}
import akka.stream.scaladsl.{FileIO, Keep, Sink, Source}
import akka.util.ByteString
import server.Matcher
import server.WorkCollatorFSM.WorkInfo

import scala.concurrent.Future

object StreamsUtil {
  /**
   *
   * @param source The source to the file that has the data that is used to match against user input
   * @param mat  stream materializer
   * @return a future that has the List of company information in a Map format for the Matcher to match.
   */
  def getCompanyData(source: Source[ByteString, Any])(implicit mat: Materializer): Future[List[Map[String, String]]] =
    getRefinedSource(source)
      .runFold(List.empty[Map[String, String]])((seed, incoming) => {
        incoming :: seed
      })

  /**
   *
   * @param byteSource  the source of user input data to perform matching
   * @param collator    the actor that keeps track of the progress
   * @param companyData the List[Map[String,String]] generated from getCompanyData method
   * @param threshold   this is to control the sensitivity of matching
   * @param sink        the stream sink to which the data has to be written
   * @param mat         stream materializer
   * @return a future that has the number of rows processed
   */
  def sendToPerformMatching(byteSource: Source[ByteString, Any], collator: ActorRef,
                            companyData: List[Map[String, String]], threshold: Double, sink: Sink[ByteString, Any])
                           (implicit mat: Materializer): Future[Long] = {
    getRefinedSource(byteSource)
      .map(x => x("id") :: x("name") :: Matcher.matchCompanies(x("name"), companyData, threshold).map(x => x.id :: x.name :: Nil).toList.flatten)
      .map(row => if (row.head == "id") List("id", "name", "matched_company_id", "matched_company_name") else row)
      .map(row => {
        collator ! WorkInfo(row)
        row
      })
      .alsoToMat(Sink.fold(0L)((i, xx) => i + 1))(Keep.right)
      .via(CsvFormatting.format())
      .toMat(sink)(Keep.left)
      .run()
  }

  /**
   * the core mapping logic for getCompanyData to convert companydata csv to List[Map[String,String]]
   *
   * @param source the input source of companyData
   * @return a refined source on which the sink could be attached.
   */
  private def getRefinedSource(source: Source[ByteString, Any]): Source[Map[String, String], Any] = {
    source.via(CsvParsing.lineScanner())
      .map(_.map(_.utf8String))
      .filterNot(x => {
        val line = x.mkString
        line != null && (line.trim.equals("") || line.trim.equals("\n"))
      })
      .map({
        case head :: second :: Nil => Map("id" -> head, "name" -> second)
        case head :: tail => Map("id" -> head, "name" -> tail.mkString(" "))
      })
  }
}
