package server

import akka.actor.{FSM, Props}
import server.WorkCollatorFSM.{AwaitingTotalCount, CollatorData, CurrentStatus, GetCurrentStatus, TotalCount, WithTotalCount, WorkInfo}

object WorkCollatorFSM{
  def props:Props = Props(new WorkCollatorFSM())

  /**
   * Finite states the actor goes thru
   */
  trait State
  case object AwaitingTotalCount extends State
  case object WithTotalCount extends State

  /**
   * The allowed commands the collater can act on
   */
  sealed trait CollatorCommand
  case class WorkInfo(ipList:List[String]) extends CollatorCommand
  case object GetCurrentStatus extends CollatorCommand
  case class TotalCount(count:Long)

  /**
   * The return data for a GetCurrentStatus request
   * @param currentTotal total number of rows processed as of now
   * @param success how many of them were a +ve match
   * @param failure how many of them were a -ve match
   * @param isFullyDone is the processing of all rows finished
   */
  case class CurrentStatus(currentTotal:Int, success:Int, failure:Int, isFullyDone:Boolean)

  /**
   * Contains the FSM data - the state of the actor
   * @param total total number of rows to process. Kept to be -1 initially to mark that actor is yet to receive total
   * @param success total successful matches incremented from the WorkInfo content
   * @param failed total failed matches incremented from the WorkInfo content
   */
  case class CollatorData(total:Long= -1, success:Int=0,failed:Int = 0)
}
class WorkCollatorFSM extends FSM[WorkCollatorFSM.State, CollatorData]{
  /**
   * Initial FSM and Data starting point
   */
  startWith(AwaitingTotalCount, CollatorData())

  /**
   * All 3 CollatorCommand commands are processed in this state
   */
  when(AwaitingTotalCount) (processWithAwaitingTotalCountFunction)

  /**
   * Only 2 commands are processed in this state. the command TotalCount is ignored as its already received.
   */
  when(WithTotalCount)(processEventsWhenTotalCountIsAlreadyKnown)

  def processWithAwaitingTotalCountFunction: PartialFunction[FSM.Event[CollatorData], FSM.State[WorkCollatorFSM.State, CollatorData]] =
    commonPartialPart.orElse({
      case Event(x: TotalCount, currentData) => goto(WithTotalCount).using(currentData.copy(total = x.count))
    })

  def processEventsWhenTotalCountIsAlreadyKnown: PartialFunction[FSM.Event[CollatorData], FSM.State[WorkCollatorFSM.State, CollatorData]] =
    commonPartialPart.orElse({
      case Event(x: TotalCount, currentData) => stay()
    })


  def commonPartialPart:PartialFunction[FSM.Event[CollatorData], FSM.State[WorkCollatorFSM.State, CollatorData]] ={
    case Event(x: WorkInfo, currentData) => {
      if (x.ipList.length > 2) {
        stay.using(currentData.copy(success = currentData.success + 1))
      } else {
        stay.using(currentData.copy(failed = currentData.failed + 1))
      }
    }
    case Event(GetCurrentStatus, currentData) =>
      stay().replying(CurrentStatus(currentTotal = currentData.success + currentData.failed,
        success = currentData.success, failure = currentData.failed,isFullyDone = (currentData.total == (currentData.success + currentData.failed))))
  }

}
