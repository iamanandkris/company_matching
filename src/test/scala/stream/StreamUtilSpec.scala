package stream

import akka.actor.{Actor, ActorRef, ActorSystem}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.testkit.{ImplicitSender, TestActors, TestKit}
import akka.util.ByteString
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent._
import scala.concurrent.duration._

class StreamUtilSpec extends TestKit(ActorSystem("MySpec"))
  with ImplicitSender
  with AnyWordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }
  val testCompanyData:String = """4047907,Jesus House
                      |3274858,Alchemy Venture Partners Limited
                      |3738566,The Coalfields Regeneration Trust
                      |3884200,Stafford Bridge Doors Limited
                      |2851746,High Fashion (UK) Limited
                      |4041459,Beyondautism
                      |5088708,Bowater Building Products Limited
                      |820660,Child Action Northwest
                      |4146446,Veolia Water Operational Services (Moray) Limited
                      |5094550,Smooth Radio Ne Limited""".stripMargin

  val testCompanyDataConverted: List[Map[String, String]] = List(Map("id" -> "4047907", "name" -> "Jesus House"),
    Map("id" -> "3274858", "name" -> "Alchemy Venture Partners Limited"),
    Map("id" -> "3738566", "name" -> "The Coalfields Regeneration Trust"),
    Map("id" -> "3884200", "name" -> "Stafford Bridge Doors Limited"),
    Map("id" -> "2851746", "name" -> "High Fashion (UK) Limited"),
    Map("id" -> "4041459", "name" -> "Beyondautism"),
    Map("id" -> "5088708", "name" -> "Bowater Building Products Limited"),
    Map("id" -> "820660", "name" -> "Child Action Northwest"),
    Map("id" -> "4146446", "name" -> "Veolia Water Operational Services (Moray) Limited"),
    Map("id" -> "5094550", "name" -> "Smooth Radio Ne Limited"))

  val testUserInputData:String = """|1,DueDil Ltd
                                   |2,Jesus House
                                   |3,Morgan Stanley UK Group
                                   |4,Google UK Limited
                                   |5,Google UK ltd
                                   |6,High Fashion (UK) Limited
                                   |7,Barclays PLC
                                   |8,Barclays Bank PLC
                                   |9,Child Action Northwest
                                   |10,barclays plc
                                   |11,sequoia Investment management Company limited
                                   |12,Sequoia Investment Ltd""".stripMargin

  "StreamUtil" must {
    "the getCompanyData function create a List[Map[String, String] as expected" in{
      val testSource = Source.single(testCompanyData).map(x => ByteString(x))
      val result = Await.result(StreamsUtil.getCompanyData(testSource), 3.seconds)
      assert(result.sortBy(x =>x("id")) == testCompanyDataConverted.sortBy(x => x("id")))
    }

    "the sendToPerformMatching function should perform the matching as expected" in{
      val testCollatorActor = system.actorOf(TestActors.echoActorProps)
      val testSource = Source.single(testUserInputData).map(x => ByteString(x))
      val result = Await.result(StreamsUtil.sendToPerformMatching(testSource, testCollatorActor,testCompanyDataConverted, 1, Sink.seq), 3.seconds)

      assert(result == 12)
    }
  }

}
