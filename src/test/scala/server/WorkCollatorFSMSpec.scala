package server

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestActors, TestFSMRef, TestKit, TestProbe}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import server.WorkCollatorFSM.{AwaitingTotalCount, CollatorData, CurrentStatus, GetCurrentStatus, TotalCount, WithTotalCount, WorkInfo}

import scala.concurrent.duration._

class WorkCollatorFSMSpec extends TestKit(ActorSystem("MySpec"))
  with ImplicitSender
  with AnyWordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "WorkCollatorFSM" must {
    "go thru different states starting from\n" +
      "1. AwaitingTotalCount with Initial State Data\n" +
      "2. On receiving WorkInfo command, it should update the state data\n" +
      "3. On receiving GetCurrentStatus command, it should return the current status\n" +
      "4. On receiving TotalCount command, it should change to WithTotalCount state and update its state data with total count" in {
      val fsm = TestFSMRef(new WorkCollatorFSM)
      val mustBeTypedProperly: TestActorRef[WorkCollatorFSM] = fsm
      assert(fsm.stateName == AwaitingTotalCount)
      assert(fsm.stateData == CollatorData())

      fsm ! WorkInfo(List("1", "Company1"))
      assert(fsm.stateName == AwaitingTotalCount)
      assert(fsm.stateData == CollatorData(-1,0,1))

      fsm ! WorkInfo(List("2", "CompanyActual", "ef234", "CompanyActual"))
      assert(fsm.stateName == AwaitingTotalCount)
      assert(fsm.stateData == CollatorData(-1,1,1))

      fsm ! GetCurrentStatus
      assert(fsm.stateName == AwaitingTotalCount)
      assert(fsm.stateData == CollatorData(-1,1,1))
      expectMsg(CurrentStatus(2,1,1,false))

      fsm ! TotalCount(4)
      assert(fsm.stateName == WithTotalCount)
      assert(fsm.stateData == CollatorData(4,1,1))

      fsm ! GetCurrentStatus
      assert(fsm.stateName == WithTotalCount)
      assert(fsm.stateData == CollatorData(4,1,1))
      expectMsg(CurrentStatus(2,1,1,false))

      fsm ! WorkInfo(List("3", "TestCompany1", "eerf234", "TestCompany1"))
      assert(fsm.stateName == WithTotalCount)
      assert(fsm.stateData == CollatorData(4,2,1))

      fsm ! WorkInfo(List("4", "TestCompany3", "efpo234", "TestCompany3"))
      assert(fsm.stateName == WithTotalCount)
      assert(fsm.stateData == CollatorData(4,3,1))

      fsm ! GetCurrentStatus
      assert(fsm.stateName == WithTotalCount)
      assert(fsm.stateData == CollatorData(4,3,1))
      expectMsg(CurrentStatus(4,3,1,true))
    }

  }




}