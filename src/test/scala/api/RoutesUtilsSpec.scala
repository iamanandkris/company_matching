package api

import java.io.File
import java.util.concurrent.TimeUnit

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpEntity.{ChunkStreamPart, Chunked}
import akka.http.scaladsl.model.headers.{ContentDispositionTypes, `Content-Disposition`}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, Multipart}
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.ByteString
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.Matcher
import org.scalatest.{BeforeAndAfterAll, fixture}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import server.WorkCollatorFSM.CurrentStatus
import stream.StreamsUtil
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.FiniteDuration

class RoutesUtilsSpec extends TestKit(ActorSystem("MySpec"))
  with ImplicitSender
  with AnyWordSpecLike
  with Matchers
  with ScalaFutures
  with BeforeAndAfterAll {

  val config = Config(FileConfig("filePathToSaveProcessedInfo", "csv", "companyDataPath"),
    HttpConfig("127.0.0.1", 3256),
    UrlEndpoints("upload", "getResult", "getStatus", "isWorkFinished", "companyMatching"))

  val testCompanyData: List[Map[String, String]] = List(Map("id" -> "4047907", "name" -> "Jesus House"),
    Map("id" -> "3274858", "name" -> "Alchemy Venture Partners Limited"),
    Map("id" -> "3738566", "name" -> "The Coalfields Regeneration Trust"),
    Map("id" -> "3884200", "name" -> "Stafford Bridge Doors Limited"),
    Map("id" -> "2851746", "name" -> "High Fashion (UK) Limited"),
    Map("id" -> "4041459", "name" -> "Beyondautism"),
    Map("id" -> "5088708", "name" -> "Bowater Building Products Limited"),
    Map("id" -> "820660", "name" -> "Child Action Northwest"),
    Map("id" -> "4146446", "name" -> "Veolia Water Operational Services (Moray) Limited"),
    Map("id" -> "5094550", "name" -> "Smooth Radio Ne Limited"))

  val testUserInputData:String = """|1,DueDil Ltd
                                    |2,Jesus House
                                    |3,Morgan Stanley UK Group
                                    |4,Google UK Limited
                                    |5,Google UK ltd
                                    |6,High Fashion (UK) Limited
                                    |7,Barclays PLC
                                    |8,Barclays Bank PLC
                                    |9,Child Action Northwest
                                    |10,barclays plc
                                    |11,sequoia Investment management Company limited
                                    |12,Sequoia Investment Ltd""".stripMargin

  implicit val testUUidGenerator : GenerateUUID = new GenerateUUID {
      override def apply(): String = "abcd"
    }

  "RoutesUtilsSpec" must {
    "return correct info for getCurrentStatusSuccessResponse" in {
      val result = RouteUtils.getCurrentStatusSuccessResponse(config)("uniqueId", CurrentStatus(10, 5, 5, false))
      val responseMessage = s"""{"uniqueId" : "uniqueId","status":{"rowsCameIn":10,"rowsProcessedSuccess":5,"rowsProcessedFailure":5},"possibleActions" : {"isWorkFinished" : "http://${config.httpConfig.host}:${config.httpConfig.port}/companyMatch/isWorkFinished/uniqueId"}}"""
      val exp = HttpResponse(entity = HttpEntity(ContentTypes.`application/json`,responseMessage))
      assert(result == exp)
    }
    "return correct info for isWorkFinishedSuccessResponse" in {
      val result = RouteUtils.isWorkFinishedSuccessResponse(config)("uniqueId", CurrentStatus(10, 5, 5, false))
      val responseMessage = s"""{"uniqueId" : "uniqueId","workFinishStatus":false,"possibleActions" : {"getResult" : "http://${config.httpConfig.host}:${config.httpConfig.port}/companyMatch/getResult/uniqueId"}}"""
      val exp = HttpResponse(entity = HttpEntity(ContentTypes.`application/json`,responseMessage))
      assert(result == exp)
    }
    "return correct info for statusInquiryFailureResult" in {
      val result = RouteUtils.statusInquiryFailureResponse("uniqueId")
      val responseMessage = s"""{"uniqueId" : "uniqueId","status":{"error":"invalid request id"}}"""
      val exp = HttpResponse(entity = HttpEntity(ContentTypes.`application/json`,responseMessage))
      assert(result == exp)
    }

    "the getCSVResponse function returns the HTTPResponse as expected" in{
      val testSource: Source[ByteString, NotUsed] = Source.single("""4047907,Jesus House""").map(x => ByteString(x))
      val expectedResponse = HttpResponse(
        headers = `Content-Disposition`(ContentDispositionTypes.inline, Map("filename" -> s"test.csv")) :: Nil,
        entity = Chunked(contentType = ContentTypes.`text/csv(UTF-8)`, chunks = testSource.map(ChunkStreamPart.apply)))

      assert(RouteUtils.getCSVResponse("test", testSource).toString == expectedResponse.toString)
    }
    "the performMatching function performs the matching and return the unique id of the request as expected" in{
      val testSource = Source.single(testUserInputData).map(x => ByteString(x))
      val inputData = Map("threshold" -> "0.5", "uploadedFile" -> testSource)
      implicit val formExtractionTimeout = FiniteDuration(3, TimeUnit.SECONDS)
      whenReady(RouteUtils.performMatching(Future.successful(inputData),Future.successful(testCompanyData),
        x => Sink.seq)){
        res => assert(res == "abcd")
      }
    }
    "the performMatching function results in an exception as the threshold is not specified" in{
      val testSource = Source.single(testUserInputData).map(x => ByteString(x))
      val inputData = Map("uploadedFile" -> testSource)
      implicit val formExtractionTimeout = FiniteDuration(3, TimeUnit.SECONDS)
      val res = RouteUtils.performMatching(Future.successful(inputData),Future.successful(testCompanyData),
        x => Sink.seq)
      assert(res.failed.futureValue.isInstanceOf[Exception])
    }

    "the performMatching function results in an exception as the upload file is not available" in{
      val testSource = Source.single(testUserInputData).map(x => ByteString(x))
      val inputData = Map("threshold" -> "0.5")
      implicit val formExtractionTimeout = FiniteDuration(3, TimeUnit.SECONDS)
      val res = RouteUtils.performMatching(Future.successful(inputData),Future.successful(testCompanyData),
        x => Sink.seq)
      assert(res.failed.futureValue.isInstanceOf[Exception])
    }

    "the validateFileExistenceAndGetTheSource function results in a Future[Source[ByteString, Any]]" in{
      val res = RouteUtils.validateFileExistenceAndGetTheSource(config, _ => true)("abcd")
      whenReady(res){x => assert(x.isInstanceOf[Source[ByteString, Any]])}
    }

    "the validateFileExistenceAndGetTheSource function results in a Future failed as the file does not exist" in{
      val res = RouteUtils.validateFileExistenceAndGetTheSource(config, _ => false)("abcd")
      assert(res.failed.futureValue.isInstanceOf[Exception])
    }
  }
}
